#include "node.cpp"
using namespace std;

class List{
        private:
                Node* ancla;
                int size;

        public:
                List();

                void init();
                void pushB(int);
                void pushE(int);
                void pushP(int, int);
                
                void popB();
                void popE();
                void popP(int);

                void empty();
                bool isEmpty();

                int searchP(int);
                Node searchV(int);
    
                //position
                int getData(int);
                Node* getAncla();
                void setAncla(Node*);
};