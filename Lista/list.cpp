#include "list.h"

List::List()
{
        this->init();
        std::cout << "Inicializando lista" << std::endl;
}

void List::init()
{
        this->ancla = NULL;
        this->size = 0;
}

void List::pushB(int value)
{
        std::cout << "Insertando nodo al inicio" << std::endl;
        Node* aux = new Node(value, this->getAncla());

        this->setAncla(aux);
        this->size++;
}

void List::pushE(int value)
{
        std::cout << "Insertando nodo al final" << std::endl;
        Node* aux = new Node(value);

        if (this->isEmpty()){
                this->setAncla(aux);
        } else {
                Node* temp = this->getAncla();
                while(temp->getPointer() != NULL){
                        temp = temp->getPointer();
                }
                temp->setPointer(aux);
        }        
        this->size++;
}

void List::pushP(int value, int pos)
{
        std::cout << "Insertando en la posicion: " << pos << std::endl;
        Node* aux = new Node(value);

        if (this->isEmpty()){
                this->pushB(value);
        } else {
                Node* temp = this->getAncla();
                Node* past;
                for (int i = 1; i < pos; ++i){
                        past = temp;
                        if(temp->getPointer() == NULL){
                                pushE(value);
                                return;
                        } else {
                                temp = temp->getPointer();
                        }
                }
                past->setPointer(aux);
                aux->setPointer(temp);
                this->size++;
        }
}


void List::popB()
{
        Node* temp = this->getAncla();

        if(!this->isEmpty()){
                this->setAncla(temp->getPointer());
                delete(temp);
                this->size--;
        }
}

void List::popE()
{
        Node* temp = this->getAncla();
        Node* past;

        if(!this->isEmpty()){
                while(temp->getPointer() != NULL){
                        past = temp;
                        temp = temp->getPointer();
                }

                past->setPointer(NULL);
                delete(temp);
                this->size--;
        }
}

void List::popP(int pos)
{
        if (this->isEmpty()){
                std::cout << "La lista ya está vacia" << std::endl;
        } else {
                Node* temp = this->getAncla();
                Node* past = temp;
                for (int i = 1; i < pos; ++i){
                        past = temp;
                        if(temp->getPointer() == NULL){
                                std::cout << "El elemento que buscas no existe" << std::endl;
                                return;
                        }
                        temp = temp->getPointer();
                }
                past->setPointer(temp->getPointer());
                delete(temp);
                this->size--;
        }
}


void List::empty()
{

}

bool List::isEmpty()
{
        return (ancla) ? false : true;
}

int List::searchP(int pos)
{
        Node* temp = this->getAncla();
        std::cout << "SIZE: " << this->size << std::endl;
        if (this->size >= pos){
                std::cout << " ----- " << std::endl;
                for (int i = 1; i < pos; ++i)
                {
                        temp = temp->getPointer();
                }
                return temp->getValue();
        }
}

Node List::searchV(int)
{

}

//position
int List::getData(int)
{

}

Node* List::getAncla()
{
        return this->ancla;
}

void List::setAncla(Node* p)
{
        this->ancla = p;
}