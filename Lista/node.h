#include <iostream>

class Node {
    private:
        int value;
        Node* pointer;

    public:
        ~Node();
        Node();
        Node(int);
        Node(int, Node*);

        int getValue();
        void setValue(int);

        Node* getPointer();
        void setPointer(Node*);
};
