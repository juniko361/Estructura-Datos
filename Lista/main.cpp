#include "list.cpp"

int main ()
{
    List* L = new List();
    Node* last = new Node();

    L->pushB(3);
    L->pushB(5);
    L->pushE(1);
    L->pushE(9);
    L->pushP(4, 3);
    L->pushP(10, 9);
    // L->popP(1);

    std::cout << L->searchP(3) << std::endl;

    // std::cout << "------- READ LIST -------" << std::endl;

    // last = L->getAncla();
    // std::cout << "Ancla: " << last << std::endl;

    // while(last != NULL){
    //     std::cout << "----------------------------" << std::endl;
    //     std::cout << "Address: " << last << std::endl;
    //     std::cout << "Value: " << last->getValue() << std::endl;
    //     std::cout << "Pointer: " << last->getPointer() << std::endl;
    //     last = last->getPointer();
    // }

    return 0;
}
