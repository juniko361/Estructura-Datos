#include <iostream>
#include <string>
#include "figura.cpp"

using namespace std;

int main(int argc, char const *argv[])
{
	Figura x;
	Figura* y = new Figura();

	x.setLado(3);
	y->setLado(8);

	x.setArea(6);
	y->setArea(1);

	cout << y->getArea() << endl;
	cout << y->getLado() << endl;
	cout << x.getArea() << endl;
	cout << x.getLado() << endl;


	return 0;
}