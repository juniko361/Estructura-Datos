#include "figura.h"

// private:
// 	float lado;
// 	float area;
// 	string nombre;

Figura::Figura(){cout << "Creando instancia de figura" << endl;}

Figura::Figura(int lado, int area, string nombre){
	this->lado = lado;
	this->area = area;
	this->nombre = nombre;
}

Figura::Figura(string nombre){
	this->nombre = nombre;
}

Figura Figura::operator+ (const Figura& f)
{
	Figura temp;
	temp.lado = lado + f.lado;
	temp.area = area + f.area;
	return temp;
}

void Figura::setLado(float lado)
{
	this->lado = lado;
}

float Figura::getLado()
{
	return this->lado;
}


void Figura::setArea(float area)
{
	this->area = area;
}

float Figura::getArea()
{
	return this->area;
}


void Figura::setNombre(string nombre)
{
	this->nombre = nombre;
}

string Figura::getNombre()
{
	return this->nombre;
}

void Figura::calcArea()
{
    this->area = this->lado*this->lado;
}

void Figura::calcArea(float lado)
{
    this->area = lado*lado;
}
