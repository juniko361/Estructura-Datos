#include <iostream>

using namespace std;

class Figura{
private:
	float lado;
	float area;
	string nombre;

public:
	Figura(int, int, string);
	Figura(string);
	Figura();

	Figura operator + (const Figura&);

	void setLado(float);
	float getLado();

	void setArea(float);
	float getArea();

	void setNombre(string);
	string getNombre();

    void calcArea();
    void calcArea(float);
};
