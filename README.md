Listas:


    - Una lista simple y lineal es un conjunto de elementos de un tipo dado que se encuentran ordenados y pueden variar en cantidad (Dinamica).
    - Permite el recorrido de todos y cada uno de sus elementos, sin saltar ninguno y en forma ordenada.

    Operaciones básicas:
        - Crear / Inicializar
        - Insertar / Push
        - Borrar / Pop
        - Vacia
        - Llena
        - Buscar
        - Leer
